package com.creation.hencetask.ui.blank

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.creation.hencetask.databinding.FragBlankBinding

class BlankFrag : Fragment() {

    lateinit var  binding : FragBlankBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragBlankBinding.inflate(inflater, container, false)

        return binding.root
    }


}