package com.creation.hencetask.ui.list_work

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.creation.hencetask.DataMap
import com.creation.hencetask.R

class ItemAdapter(var list: List<DataMap>) :
RecyclerView.Adapter<ItemAdapter.MainViewHolder>() {


    lateinit var clicked: MainInterface

    interface MainInterface {
        fun clickedSelected(position: Int)
    }

    fun onClicked(clicked: MainInterface)
    {this.clicked = clicked}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {

        val view  = LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)

        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        holder.tvName.text = "Item Name - ${list[position].title}"
        holder.ivDelete.setImageResource(R.drawable.ic_delete)
        holder.ivDelete.setOnClickListener{
            clicked.clickedSelected(position)
        }

    }


    inner class MainViewHolder(v: View) : RecyclerView.ViewHolder(v){

       var tvName : TextView = v.findViewById(R.id.tvName)

       var ivDelete : ImageView = v.findViewById(R.id.ivDelete)


    }

}