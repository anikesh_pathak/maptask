package com.creation.hencetask.ui.list_work

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.creation.hencetask.MainActivity

import com.creation.hencetask.databinding.FragListBinding

class ListFrag : Fragment() {

    val listFragVM: ListFragVM by viewModels()
    lateinit var binding: FragListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragListBinding.inflate(inflater, container, false)

        val adapter = ItemAdapter((context as MainActivity).list)
        binding.rvMain.adapter = adapter
        adapter.onClicked(object :ItemAdapter.MainInterface{
            override fun clickedSelected(position: Int) {


                        (context as MainActivity).list.removeAt(position)
                        adapter.notifyItemRemoved(position)
                        adapter.notifyItemRangeChanged(position,(context as MainActivity).list.size)


                }



        })



        return binding.root
    }

}