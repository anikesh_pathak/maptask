package com.creation.hencetask.ui.map_work

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.creation.hencetask.DataMap
import com.creation.hencetask.MainActivity
import com.creation.hencetask.R
import com.creation.hencetask.databinding.FragMapBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions

class MapFrag : Fragment(),OnMapReadyCallback {

    lateinit var binding: FragMapBinding
    lateinit var mMap: GoogleMap
     val mapFragVM: MapFragVM by viewModels()



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragMapBinding.inflate(inflater, container, false)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this@MapFrag)
        return  binding.root
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMapClickListener { p0 ->
            val list2 = (context as MainActivity).list
            mMap.addMarker(
                MarkerOptions()
                    .position(p0)
                    .title("${p0.latitude}-${p0.longitude}")
                    .icon(BitmapDescriptorFactory.defaultMarker())
                    .draggable(false)
                    .zIndex((list2.size + 1).toFloat())
            )!!.tag = "${p0.latitude}-${p0.longitude}"
            (context as MainActivity).list.add(DataMap(p0,"${p0.latitude}-${p0.longitude}"))
        }
        if((context as MainActivity).list.isNotEmpty()) {
            val boundsBuilder = LatLngBounds.Builder()
            ((context as MainActivity).list.indices).map {
                boundsBuilder.include((context as MainActivity).list[it].position)
            }
            val bounds = boundsBuilder.build()
            with(mMap) {
                moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 30))
            }
            addMarkersToMap()


        }
    }
    private fun addMarkersToMap() {

        ((context as MainActivity).list.indices).map {
            mMap.addMarker(
                MarkerOptions()
                    .position((context as MainActivity).list[it].position)
                    .title((context as MainActivity).list[it].title)
                    .icon(BitmapDescriptorFactory.defaultMarker())
                    .draggable(false)
                    .zIndex(it.toFloat()))!!.tag = it.toString()
        }
    }

}

